# Progress Log
---
| Day | Date | Technical Indicator |
| --- | ---- | ------------------- |
| 001 | 14-02-18 | welcome and theory lectures of coursera audio-signal-processing course |
| 002 | 15-02-18 | demonstration, programming lectures of coursera audio-signal-processing course |
| 003 | 16-02-18 | Programming Assignments of Week 1 |
| 004 | 17-02-18 | Theory lectures of Week 2 |
| 005 | 18-02-18 | Demonstration and Programming lectures of Week 2 |
| 006 | 19-02-18 | Programming Assignments of Week 2 |
| 007 | 20-02-18 | Theory lectures of Week 3 |
| 008 | 21-02-18 | Demonstration and Programming lectures of Week 3 |
| 009 | 22-02-18 | Did some Audio Processing on octave |
| 010 | 23-02-18 | Working on Week 3 Programming Assignments |
| 011 | 25-02-18 | Completed Week 3 Programming Assignments |
| 012 | 26-02-18 | Theory and Demonstration lectures of Week 4 |
| 013 | 27-02-18 | Programming lectures of Week 4 |
| 014 | 28-02-18 | Started with Programming assignments Week 4 |
